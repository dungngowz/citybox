<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\Game;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get( '/', function () {
    return view( 'welcome' );
} );

Route::get( '/render-password', function () {
    return Hash::make( '12345678' );
} );

Route::get('update-column', function() {
    $customers = Customer::all();

    $customers->each(function($item, $key) {
        $id = $item->id;
        $game1 = Game::where( 'customer_id', $id )->where( 'level_id', 1)->first();
        $game2 = Game::where( 'customer_id', $id )->where( 'level_id', 2)->first();
        $game3 = Game::where( 'customer_id', $id )->where( 'level_id', 3)->first();
        $game4 = Game::where( 'customer_id', $id )->where( 'level_id', 4)->first();

        if($game1 && $game2 && $game3 && $game4) {
            if($game4->times < $game3->times || $game3->times < $game2->times || $game2->times < $game1->times) {
                $item->update([
                    'is_cheat' => 1
                ]);
            } else {
                $item->update([
                    'is_cheat' => 0
                ]);
            }
        }
    });
});
