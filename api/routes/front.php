<?php

namespace App\Http\Controllers\Front;

use Illuminate\Support\Facades\Route;

Route::group( ['prefix' => 'front'], function () {
    Route::post( 'auth/login-social', [AuthController::class, 'loginSocial'] );

    Route::group( ['middleware' => 'auth.jwt'], function () {
        Route::get( 'auth/user', [AuthController::class, 'me'] );
    } );

    Route::get( 'get-game/{id}', [GameController::class, 'getGame'] );
    Route::get( 'games', [GameController::class, 'getAllGame'] );
    Route::get( 'add-coins', [GameController::class, 'addCoins']);
    Route::post( 'update-game', [GameController::class, 'updateGame'] );
    Route::post( 'update-solution', [GameController::class, 'updateSolution'] );

    Route::get( 'ranks', [CustomerController::class, 'getRank']);
    Route::get( 'customer', [CustomerController::class, 'getCurrentCustomer']);
    Route::post( 'update-info', [CustomerController::class, 'updateInfo']);
    Route::post( 'check-name', [CustomerController::class, 'checkName']);
    Route::get( 'play-again', [CustomerController::class, 'playAgain']);

    Route::get( 'settings', [SettingController::class, 'index'] );

    Route::get('solutions/{id}', [SolutionController::class, 'getSolution']);
    Route::post('solutions/check', [SolutionController::class, 'checkSolution']);
} );
