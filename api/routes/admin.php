<?php

namespace App\Http\Controllers\Admin;

use App\Models\Solution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group( ['prefix' => 'admin/auth'], function () {
    Route::post( 'login', [AuthController::class, 'login'] );

    Route::group( ['middleware' => 'auth.jwt'], function () {
        Route::get( 'logout', [AuthController::class, 'logout'] );
        Route::get( 'user', [AuthController::class, 'me'] );
    } );
} );

Route::group( ['prefix' => 'admin'], function () {
    Route::get( 'customers', [CustomerController::class, 'index'] );
    Route::get( 'customers/csv', [CustomerController::class, 'csv'] );
    Route::get( 'customers/{id}/delete', [CustomerController::class, 'delete'] );

    Route::get( 'games', [GameController::class, 'index'] );

    Route::get( 'ranks', [RankController::class, 'index'] );
    Route::get( 'ranks/csv', [RankController::class, 'csv'] );

    Route::get('settings', [SettingController::class, 'index']);
    Route::post('settings/{type}', [SettingController::class, 'update']);


    Route::get('solutions', [SolutionController::class, 'index']);
    Route::post('solutions/{id}', [SolutionController::class, 'update']);

    Route::get( '/selection/levels', function () {
        return \App\Models\Level::query()->pluck( 'name', 'id' )->map( function (
            $v,
            $i
        ) {
            return ['text' => $v, 'value' => $i];
        } )->values();
    } );
} );
