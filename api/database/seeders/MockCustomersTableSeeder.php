<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MockCustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Customer::factory()->count(30)->create();
    }
}
