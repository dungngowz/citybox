<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => 1,
            'name' => 'City Box',
            'email' => 'admin@gmail.com',
            'password' => Hash::make(123456),
            'deleted_at' => NULL,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        \App\Models\User::firstOrCreate($data);
    }
}
