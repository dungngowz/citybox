<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => 1,
            'misson01' => '{"question":"Căn phòng của bạn hiện đang có tình trạng ẩm mốc, đâu là cách giải quyết nhanh vấn đề này?","answer1":"Quạt hút","answer2":"Quạt","answer3":"Máy lọc không khí","answer4":"Máy lạnh","correct_answer":0}',
            'misson02' => 'https://www.youtube.com/watch?v=m1FATWbvoLQ&list=PLEBCuRnMaqHpG-xnfWijBhqNHvlaAlcsj&ab_channel=PanasonicVietnam',
            'deleted_at' => NULL,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        \App\Models\Setting::firstOrCreate($data);
    }
}
