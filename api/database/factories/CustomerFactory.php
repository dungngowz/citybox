<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $passDefault = env( 'PASSWORD_DEFAULT', 's5d123ds14' );

        return [
            'id_social'  => $this->faker->numerify( '##########' ),
            'name'       => $this->faker->name,
            'email'      => $this->faker->unique()->safeEmail,
            'phone'      => $this->faker->numerify( '##########' ),
            'password'   => Hash::make( $passDefault ),
            'gender'     => Arr::random( [1, 2] ),
            'times'      => rand( 1, 10 ),
            'coins'      => rand( 100, 300 ),
            'ip'         => mt_rand( 0, 255 ) . '.' . mt_rand( 0, 255 ) . '.' . mt_rand( 0, 255 ) . '.' . mt_rand( 0, 255 ),
            'deleted_at' => null,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
