/*
 Navicat Premium Data Transfer

 Source Server         : Aws Dung Ngo
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : citybox_db

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 07/10/2021 22:45:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admins
-- ----------------------------
BEGIN;
INSERT INTO `admins` VALUES (1, 'City Box', 'admin@gmail.com', NULL, '$2y$10$S4y9c.B2p6fiqZ4tcxlLie058Bxz2qyWyCpC30KYtfT2iHsCOyV7q', NULL, '2021-09-28 14:49:23', '2021-09-28 14:49:23', NULL);
COMMIT;

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_social` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `times` int DEFAULT NULL,
  `coins` int DEFAULT NULL,
  `type_social` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of customers
-- ----------------------------
BEGIN;
INSERT INTO `customers` VALUES (9, '10224538414904342', 'Lê Quang', 'leducquang.ent', 'leducquang.ent@gmail.com', NULL, NULL, '$2y$10$G8SaXU0m/lQLOA48VzGFue/Muai6obVhLY1u/kiNnms5zO6bq/dla', NULL, 400, 'facebook', NULL, NULL, 'EAAtG2dHWVVQBAGL3yxZB1kcHFYn84WHoT7nwhbL8NPziZCB4YbsZBfaYR5gF9Kut1bK6JC63BHOZAef8WXuTZAT0eYL709tLuRjZAAbPl7sF4K5tYsUBZCIvoMC2kWzGOVgElsCwgR6E2EKPaHdZCEHeM8LafBGMZCyFQwOr7wEuiJVGXaDz4mQUvv7mOw3EVgPZAMTx5vxnUCZC1OJIZCVp5MpdpO9Cos53vfIZD', '2021-10-05 15:33:27', '2021-10-05 15:33:27', NULL);
INSERT INTO `customers` VALUES (12, '2003258966499755', 'Trần Lợi', 'loitran1325', 'loitran1325@gmail.com', NULL, NULL, '$2y$10$2yT8dA5BjoIb1ObIR1tDoOrnKBlAPj74QlU7TvP50XNIfGyPiiCfC', NULL, 400, 'facebook', NULL, NULL, 'EAAtG2dHWVVQBAKTXCxP352z0MpjUrOvhsWSAVm1ARsQ9rZAixrMnssj28S6t3BTXG5HClJvxGYJ5rXuj9SfZCQiqagZCAyJWSiz7AFyTt2LZCOzekGrWcB8QL14D2kxhMEAVAGpnUZB1savZAeh6ia6oenrBglfhrfhWnq4lg2VPv11TQsRCqHvQ6UWtQhxlzA9zX8sVrR0mZCLZBZBMX2OSN', '2021-10-06 09:42:18', '2021-10-06 09:42:18', NULL);
INSERT INTO `customers` VALUES (13, '4499718490074935', 'Lân Phạm', 'nguoi_yeu_hoang_anh_11a3', 'nguoi_yeu_hoang_anh_11a3@yahoo.com', NULL, NULL, '$2y$10$XIsdlb/fG9BLpSJLlisBAue.vbcQtdX0iX2mvesdd2WtRx0R.Hxh6', NULL, 350, 'facebook', '171.240.138.128', NULL, 'EAAtG2dHWVVQBALNM4ZAMOZC9WtWTFxmEleDTpGMQpzvZBWtElrxSW9CBXwnLszDqlobVwCuDAbUUemlxzH8Ly6dkIfXsZCJp6rafvhaRdibPm6kbBsAhUJZCZCircqS0B2lsm4NZCIo6EG0mcebV7dmRtpng4123t2FfSZBzGXshILArEZCaWM1WLhrUQ0RKBZCUAZAS6fXkFBwkGxfK8RkiR31KQIlqrjFgbcZD', '2021-10-06 12:46:26', '2021-10-06 12:46:26', NULL);
INSERT INTO `customers` VALUES (14, '6203380519732082', 'Lã Dũng', 'la.tiendung88', 'la.tiendung88@gmail.com', NULL, NULL, '$2y$10$tix/Jh0cz5ar9R3QoWO9YeaID4Qvar/yzYgq6oHDGGTxqPkSxqSua', NULL, 400, 'facebook', NULL, NULL, 'EAAtG2dHWVVQBANcsjd1WO1Ev2AQfEcr41LO64ZAtTRivkSQXW2ZCWB41lzRtiEnRehRZCK5efvQBAEMM6ZA0FGlUd25bdZAGVuQ8tVOW7sSy5fQnyEnOi4hZCei1AFNJahXHD0AiuxQhPazn21NbTNLQ5g0CzQbaceaqcWZBiIwe2JnTJ0OYxoj3LPmBwEbZAYIHhADvcNp5ipyTjH7lmMRiZB3p6FqVjuWsZD', '2021-10-06 14:59:52', '2021-10-06 14:59:52', NULL);
INSERT INTO `customers` VALUES (15, '1003436037108982', 'Ngô Dũng', 'dung.ngowz', 'dung.ngowz@gmail.com', NULL, NULL, '$2y$10$xZ8kbyaennzqzktKMfNux.4K.nJP2TrPTh3RO5FuBiMhdpQkkrKFO', NULL, 400, 'facebook', NULL, NULL, 'EAAtG2dHWVVQBAPQVcCQSPv2EwloKiuu7ZC2ZCdsldBGm6yX7D2skDS1mcZC5BLtVCmNjgZCZAtyKXosCIf3Cl3sFLGTPsP4ZA0qm0LZAJnw9vrxY0gFhyj0t8bgkkSZCbT23TNd2wcy6iVGpaZAlZAFSnGL3Vi7ZCZBw720X7fPMrcJQFlA0U0Ex0emSVkyRHkTML91rHj3YNKvlomW8COHxfQZCR', '2021-10-07 15:25:48', '2021-10-07 15:25:48', NULL);
COMMIT;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for games
-- ----------------------------
DROP TABLE IF EXISTS `games`;
CREATE TABLE `games` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint unsigned NOT NULL,
  `level_id` bigint unsigned DEFAULT NULL,
  `meta_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `coins_berore_play` int DEFAULT NULL,
  `coins_after_play` int DEFAULT NULL,
  `times` int DEFAULT NULL,
  `is_done` tinyint(1) DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `solution` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id_fk` (`customer_id`),
  KEY `level_id_fk_play` (`level_id`),
  CONSTRAINT `customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `level_id_fk_play` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of games
-- ----------------------------
BEGIN;
INSERT INTO `games` VALUES (11, 9, 1, NULL, 400, 400, NULL, 0, '14.226.239.133', NULL, '2021-10-05 15:33:27', '2021-10-05 15:33:27', NULL);
INSERT INTO `games` VALUES (12, 9, 1, '[5]', 400, 380, 4, 0, '14.226.239.133', NULL, '2021-10-05 15:33:46', '2021-10-05 15:33:46', NULL);
INSERT INTO `games` VALUES (13, 9, 1, '[5,6]', 400, 350, 8, 0, '14.226.239.133', NULL, '2021-10-05 15:33:50', '2021-10-05 15:33:50', NULL);
INSERT INTO `games` VALUES (14, 9, 1, '[5,6,7]', 400, 290, 10, 0, '14.226.239.133', NULL, '2021-10-05 15:33:52', '2021-10-05 15:33:52', NULL);
INSERT INTO `games` VALUES (15, 9, 1, '[5,6,7,8]', 400, 210, 12, 0, '14.226.239.133', NULL, '2021-10-05 15:33:55', '2021-10-05 15:33:55', NULL);
INSERT INTO `games` VALUES (16, 9, 1, '[5,6,7,8,9]', 400, 70, 14, 0, '14.226.239.133', NULL, '2021-10-05 15:33:56', '2021-10-05 15:33:56', NULL);
INSERT INTO `games` VALUES (19, 12, 1, NULL, 400, 400, NULL, 0, '118.70.150.136', NULL, '2021-10-06 09:42:18', '2021-10-06 10:39:01', NULL);
INSERT INTO `games` VALUES (21, 13, 1, '[5,6,7,9,8]', 350, 110, 25, 0, '171.240.138.128', 'Quạt âm tường', '2021-10-06 12:46:26', '2021-10-06 12:46:49', NULL);
INSERT INTO `games` VALUES (22, 14, 1, '[5,6,7,8,9]', 400, 110, 22, 0, '113.23.115.221', 'Quạt âm trần', '2021-10-06 14:59:52', '2021-10-06 15:00:17', NULL);
INSERT INTO `games` VALUES (23, 15, 1, NULL, 400, 400, NULL, 0, '171.247.10.127', NULL, '2021-10-07 15:25:48', '2021-10-07 15:25:48', NULL);
COMMIT;

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `level_id` bigint unsigned DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coins` int DEFAULT NULL,
  `background` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_active` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `level_id_fk` (`level_id`),
  CONSTRAINT `level_id_fk` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of items
-- ----------------------------
BEGIN;
INSERT INTO `items` VALUES (5, 1, 'Bồn tắm', NULL, 20, NULL, NULL, '2021-09-25 21:06:04', '2021-09-25 21:06:04', '2021-09-25 21:06:04');
INSERT INTO `items` VALUES (6, 1, 'Bồn vệ sinh', NULL, 30, NULL, NULL, '2021-09-25 21:06:13', '2021-09-25 21:06:13', '2021-09-25 21:06:13');
INSERT INTO `items` VALUES (7, 1, 'Bồn rữa mặt', NULL, 60, NULL, NULL, '2021-09-25 21:06:29', '2021-09-25 21:06:29', '2021-09-25 21:06:29');
INSERT INTO `items` VALUES (8, 1, 'Tủ đồ', NULL, 80, NULL, NULL, '2021-09-25 21:06:36', '2021-09-25 21:06:36', '2021-09-25 21:06:36');
INSERT INTO `items` VALUES (9, 1, 'Gương', NULL, 100, NULL, NULL, '2021-09-25 21:06:49', '2021-09-25 21:06:49', '2021-09-25 21:06:49');
INSERT INTO `items` VALUES (10, 1, 'Vòi sen', NULL, 120, NULL, NULL, '2021-09-25 21:06:56', '2021-09-25 21:06:56', '2021-09-25 21:06:56');
INSERT INTO `items` VALUES (11, 2, 'Tủ bếp', NULL, 20, NULL, NULL, '2021-09-25 22:04:15', '2021-09-25 22:04:15', '2021-09-25 22:04:15');
INSERT INTO `items` VALUES (12, 2, 'Bàn ăn', NULL, 30, NULL, NULL, '2021-09-25 22:04:25', '2021-09-25 22:04:25', '2021-09-25 22:04:25');
INSERT INTO `items` VALUES (13, 2, 'Thảm sàn', NULL, 60, NULL, NULL, '2021-09-25 22:04:33', '2021-09-25 22:04:33', '2021-09-25 22:04:33');
INSERT INTO `items` VALUES (14, 2, 'Kệ bếp', NULL, 80, NULL, NULL, '2021-09-25 22:04:48', '2021-09-25 22:04:48', '2021-09-25 22:04:48');
INSERT INTO `items` VALUES (15, 2, 'Kệ chén', NULL, 100, NULL, NULL, '2021-09-25 22:04:58', '2021-09-25 22:04:58', '2021-09-25 22:04:58');
INSERT INTO `items` VALUES (16, 2, 'Tù lạnh', NULL, 120, NULL, NULL, '2021-09-25 22:05:05', '2021-09-25 22:05:05', '2021-09-25 22:05:05');
INSERT INTO `items` VALUES (17, 3, 'Giường ngủ', NULL, 20, NULL, NULL, '2021-09-25 22:05:21', '2021-09-25 22:05:21', '2021-09-25 22:05:21');
INSERT INTO `items` VALUES (18, 3, 'Chăn gối', NULL, 30, NULL, NULL, '2021-09-25 22:05:29', '2021-09-25 22:05:29', '2021-09-25 22:05:29');
INSERT INTO `items` VALUES (19, 3, 'Nệm', NULL, 60, NULL, NULL, '2021-09-25 22:05:35', '2021-09-25 22:05:35', '2021-09-25 22:05:35');
INSERT INTO `items` VALUES (20, 3, 'Tủ đồ', NULL, 80, NULL, NULL, '2021-09-25 22:05:48', '2021-09-25 22:05:48', '2021-09-25 22:05:48');
INSERT INTO `items` VALUES (21, 3, 'Đèn ngủ', NULL, 100, NULL, NULL, '2021-09-25 22:05:57', '2021-09-25 22:05:57', '2021-09-25 22:05:57');
INSERT INTO `items` VALUES (22, 3, 'Bàn trang điểm', NULL, 120, NULL, NULL, '2021-09-25 22:06:06', '2021-09-25 22:06:06', '2021-09-25 22:06:06');
INSERT INTO `items` VALUES (23, 4, 'Sofa', NULL, 20, NULL, NULL, '2021-09-25 22:06:31', '2021-09-25 22:06:31', '2021-09-25 22:06:31');
INSERT INTO `items` VALUES (24, 4, 'Bàn sofa', NULL, 30, NULL, NULL, '2021-09-25 22:06:39', '2021-09-25 22:06:39', '2021-09-25 22:06:39');
INSERT INTO `items` VALUES (25, 4, 'Kệ sách', NULL, 60, NULL, NULL, '2021-09-25 22:06:48', '2021-09-25 22:06:48', '2021-09-25 22:06:48');
INSERT INTO `items` VALUES (26, 4, 'Tivi', NULL, 80, NULL, NULL, '2021-09-25 22:06:59', '2021-09-25 22:06:59', '2021-09-25 22:06:59');
INSERT INTO `items` VALUES (27, 4, 'Kệ tivi', NULL, 100, NULL, NULL, '2021-09-25 22:07:09', '2021-09-25 22:07:09', '2021-09-25 22:07:09');
INSERT INTO `items` VALUES (28, 4, 'Chậu cây', NULL, 120, NULL, NULL, '2021-09-25 22:07:17', '2021-09-25 22:07:17', '2021-09-25 22:07:17');
COMMIT;

-- ----------------------------
-- Table structure for levels
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of levels
-- ----------------------------
BEGIN;
INSERT INTO `levels` VALUES (1, 'Phòng tắm', 'phong-tam', '2021-09-25 21:00:33', '2021-09-25 21:00:33', '2021-09-25 21:00:33');
INSERT INTO `levels` VALUES (2, 'Phòng bếp', 'phong-bep', '2021-09-25 21:01:01', '2021-09-25 21:01:01', '2021-09-25 21:01:01');
INSERT INTO `levels` VALUES (3, 'Phòng ngủ', 'phong-ngu', '2021-09-25 21:01:13', '2021-09-25 21:01:13', '2021-09-25 21:01:13');
INSERT INTO `levels` VALUES (4, 'Phòng khách', 'phong-khach', '2021-09-25 21:01:28', '2021-09-25 21:01:28', '2021-09-25 21:01:28');
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` bigint NOT NULL,
  `misson01` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `misson02` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
BEGIN;
INSERT INTO `settings` VALUES (1, '{\"question\":\"Căn phòng của bạn hiện đang có tình trạng ẩm mốc, đâu là cách giải quyết nhanh vấn đề này?\",\"answer1\":\"Quạt hút\",\"answer2\":\"Quạt\",\"answer3\":\"Máy lọc không khí\",\"answer4\":\"Máy lạnh\",\"correct_answer\":0}', 'https://www.youtube.com/watch?v=m1FATWbvoLQ&list=PLEBCuRnMaqHpG-xnfWijBhqNHvlaAlcsj&ab_channel=PanasonicVietnam', '2021-10-03 10:42:37', '2021-10-03 14:11:04', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
