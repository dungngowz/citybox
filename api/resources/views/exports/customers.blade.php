<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Facebook ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Gender</th>
            <th>Times</th>
            <th>Coins</th>
            <th>IP</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($query->get() as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->fb_id }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->phone }}</td>
            <td>{{ $item->gender }}</td>
            <td>{{ $item->times }}</td>
            <td>{{ $item->coins }}</td>
            <td>{{ $item->ip }}</td>
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->updated_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
