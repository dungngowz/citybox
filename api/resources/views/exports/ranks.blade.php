<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Actor Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Time</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($query->get() as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->customer->actor_name }}</td>
            <td>{{ $item->customer->email }}</td>
            <td>{{ $item->customer->phone }}</td>
            <td>{{ $item->times }}</td>
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->updated_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
