<?php

namespace App\Models;

use App\Models\BaseModel;

class Setting extends BaseModel
{

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['mission01', 'mission02', 'mission03', 'created_at', 'updated_at', 'deleted_at'];
}
