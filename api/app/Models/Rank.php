<?php

namespace App\Models;

use App\Models\BaseModel;

class Rank extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    protected $append = ['created_at_display'];

    /**
     * @var array
     */
    protected $fillable = ['customer_id', 'times', 'created_at', 'updated_at', 'deleted_at'];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function getCreatedAtDisplayAttribute()
    {
        return date('Y/m/d H:i:s', strtotime($this->created_at));
    }
}
