<?php

namespace App\Models;

use App\Models\BaseModel;


class Solution extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    protected $append = ['is_correct_display'];

    /**
     * @var array
     */
    protected $fillable = ['level_id', 'name', 'desc', 'is_correct', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo('App\Models\Level');
    }

    public function getIsCorrectDisplayAttribute()
    {
        return $this->is_correct == 1 ? 'True' : 'False';
    }
}
