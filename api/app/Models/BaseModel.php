<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModel extends Model
{
    // use SoftDeletes;

    protected $appends = ['updated_at_display', 'created_at_display'];

    protected $hidden = ['deleted_at'];

    public function getUpdatedAtDisplayAttribute()
    {
        return date('Y/m/d H:i:s', strtotime($this->updated_at));
    }

    public function getCreatedAtDisplayAttribute()
    {
        return date('Y/m/d H:i:s', strtotime($this->created_at));
    }

    public function setUpdatedAtAttribute()
    {
        $this->attributes['updated_at'] = date('Y-m-d H:i:s');
    }
}
