<?php

namespace App\Models;

use App\Models\BaseModel;

class CustomerTmp extends BaseModel
{
    protected $table = 'customers_tmp';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    public $timestamps = false;
}
