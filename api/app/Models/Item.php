<?php

namespace App\Models;

use App\Models\BaseModel;

/**
 * @property integer $id
 * @property integer $level_id
 * @property string $name
 * @property string $icon
 * @property int $coins
 * @property string $background
 * @property string $background_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Level $level
 */
class Item extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['level_id', 'name', 'icon', 'coins', 'background', 'background_active', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo('App\Models\Level');
    }
}
