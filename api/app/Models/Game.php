<?php

namespace App\Models;

use App\Models\BaseModel;

/**
 * @property integer $id
 * @property integer $customer_id
 * @property integer $level_id
 * @property string $meta_data
 * @property int $coins_berore_play
 * @property int $coins_after_play
 * @property int $times
 * @property boolean $is_done
 * @property string $ip
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Customer $customer
 * @property Level $level
 */
class Game extends BaseModel
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    protected $append = ['created_at_display'];

    /**
     * @var array
     */
    protected $fillable = ['customer_id', 'level_id', 'meta_data', 'coins_berore_play', 'coins_after_play', 'times', 'is_done', 'solution', 'ip', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo('App\Models\Level');
    }

    public function getCreatedAtDisplayAttribute()
    {
        return date('Y/m/d H:i:s', strtotime($this->created_at));
    }
}
