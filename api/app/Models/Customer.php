<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property integer $id
 * @property string $fb_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property boolean $gender
 * @property int $times
 * @property int $coins
 * @property string $ip
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Game[] $games
 */
class Customer extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    protected $append = ['gender_display', 'created_at_display'];

    /**
     * @var array
     */
    protected $fillable = ['id_social', 'name', 'email', 'phone', 'username', 'password', 'gender', 'times', 'coins', 'ip', 'created_at', 'updated_at', 'deleted_at', 'avatar', 'token', 'type_social', 'actor_name', 'sms_sent', 'is_cheat'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function games()
    {
        return $this->hasMany( 'App\Models\Game' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ranks()
    {
        return $this->hasMany( 'App\Models\Rank' );
    }

    public function getGenderDisplayAttribute()
    {
        return $this->gender == 1 ? 'Male' : 'Female';
    }

    public function getCreatedAtDisplayAttribute()
    {
        return date('Y/m/d H:i:s', strtotime($this->created_at));
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
