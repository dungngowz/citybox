<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
        $classExp = get_class($e);

        if ($classExp == 'Illuminate\Auth\AuthenticationException') {
            return response()->json([
                'code' => 401,
                'data' => [],
                'message' => __('text.please_login_to_perform_this_function'),
                'errors' => ''
            ], 401);
        } else if ($classExp == 'Twilio\Exceptions\RestException') {
            return response()->json([
                'code' => 422,
                'data' => [],
                'message' => __('text.your_phone_number_is_not_valid'),
                'errors' => ''
            ], 422);
        }

        return parent::render($request, $e);
    }
}
