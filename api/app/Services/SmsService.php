<?php

namespace App\Services;

class SmsService
{
    private $sms_u = 'movement';
    private $sms_pwd = 'yus2h';
    private $sms_brandName = 'TEST-DS';

    public function __construct()
    {
    }

    public function send($phoneNumber = '', $sms = '') {
        $curl = curl_init();

        $requestData = array(
            'from' => $this->sms_brandName,
            'u' => $this->sms_u,
            'pwd' => $this->sms_pwd,
            'phone' => $phoneNumber,
            'sms' => $sms,
            'bid' => $this->sms_brandName . '_' . $phoneNumber,
            'type' => '8',
            'json' => '1',
        );

        \Log::debug(json_encode($requestData));

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://cloudsms.vietguys.biz:4438/api/index.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $requestData,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        \Log::debug(json_encode($response));

        $resp = json_decode($response);

        if (!empty($resp->error) && $resp->error == 0) {
            return true;
        }

        return false;
    }

    public function topUp($phoneNumber, $amount = '20000') {
        $curl = curl_init();

        $requestData = array(
            'u' => $this->sms_u ,
            'pwd' => $this->sms_pwd ,
            'phone' => $phoneNumber ,
            'tid' => 'tu_' . $phoneNumber ,
            'amount' => $amount ,
            'json' => '1',
        );

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://cloudsms2.vietguys.biz:4438/api/topup/index.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $requestData
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        \Log::debug('sms_topup' ,[json_encode($requestData), $response]);

        $resp = json_decode($response);

        if (!empty($resp->error) && $resp->error == 0) {
            return true;
        }

        return false;
    }
}
