<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CsvExport implements FromView
{
    protected $query;
    protected $view;

    /**
     * Constructor
     *
     * @param Collection $fields
     * @param Builder $query
     * @param string $view
     */
    public function __construct(Builder $query, string $view)
    {
        $this->query = $query;
        $this->view = $view;
    }

    /**
    * @return View
    */
    public function view(): View
    {
        return view($this->view, [
            'query' => $this->query
        ]);
    }
}
