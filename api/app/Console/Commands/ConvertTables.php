<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;use Illuminate\Support\Facades\Artisan;use Illuminate\Support\Facades\DB;use Illuminate\Support\Facades\File;use Illuminate\Support\Facades\Log;use Illuminate\Support\Str;

class ConvertTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Turn tables in database into models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tables = DB::select('SHOW TABLES');
        $this->info('Created:');
        $i = 1;
        foreach($tables as $item)
        {
            $table_name = head($item);
            if($table_name != 'migrations' && $table_name != 'failed_jobs' && $table_name != 'password_resets'){

                $model = Str::singular($table_name);
                $model = ucwords($model, '_');
                $model = str_replace('_', '', $model);
                Artisan::call('krlove:generate:model '. $model . ' --table-name=' . $table_name);
                $this->info("\t". ($i) .": " .$model);
                $i++;
            }
        }
    }
}
