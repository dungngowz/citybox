<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'customers'    => $this->customer,
            'levels'      => $this->level,
            'meta_data'     => $this->meta_data,
            'coins_berore_play'      => $this->coins_berore_play,
            'coins_after_play'       => $this->coins_after_play,
            'times'       => $this->times,
            'is_done'       => $this->is_done,
            'solution'      => $this->solution,
            'ip' => $this->ip,
            'created_at_display'   => $this->created_at_display,
        ];
    }
}
