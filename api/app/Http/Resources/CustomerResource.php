<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray( $request )
    {
        return [
            'id'                 => $this->id,
            'name'               => $this->name,
            'username'           => $this->username,
            'actor_name'         => $this->actor_name,
            'gender'             => $this->gender,
            'gender_display'     => $this->gender_display,
            'email'              => $this->email,
            'phone'              => $this->phone,
            'times'              => $this->times,
            'coins'              => $this->coins,
            'type_social'        => $this->type_social,
            'id_social'          => $this->id_social,
            'created_at_display' => $this->created_at_display,
        ];
    }
}
