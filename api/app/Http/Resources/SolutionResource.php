<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SolutionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'level_id'    => $this->level_id,
            'desc'      => $this->desc,
            'is_correct_display'      => $this->is_correct_display,
            'is_correct'      => $this->is_correct,
        ];
    }
}
