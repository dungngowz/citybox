<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CsvExport;
use App\Http\Controllers\Admin\Traits\Searchable;
use App\Http\Controllers\Controller;
use App\Http\Resources\RankCollection;
use App\Models\Rank;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class RankController extends Controller
{
    use Searchable;
    /**
     * Display a listing of the Customer.
     *
     * @return Response
     */
    public function index()
    {
        $options = $this->dataTableOptions();

        $query = $this->prepareSearchQuery( Rank::query());

        $records = $query->paginate( $options->get( 'itemsPerPage' ) );

        return $this->response( 200, [
            'records' => new RankCollection( $records ),
        ] );
    }

    /**
     * CSV download
     *
     * @param  Rank   $customer
     * @return Response
     */
    public function csv()
    {
        $options = $this->dataTableOptions();
        $query   = $this->prepareSearchQuery( Rank::query(), $options );

        return Excel::download( new CsvExport( $query, 'exports.ranks' ), '_.csv' );
    }

    /**
     * Prepare search query builder instance
     *
     * @param  Collection $options
     * @return Builder
     */
    public function prepareSearchQuery(Builder $query): Builder
    {
        $query   = $this->searchQuery( $query );

        return $query->orderBy('times')->orderBy('created_at')->whereNotNull('times')->whereHas('customer', function ($q) {
            $q->where('is_cheat', 0);
        })->with('customer');
    }

    public function searchQuery( Builder $query )
    {
        $items = !empty( request()->input( 'search' ) ) ? json_decode( request()->input( 'search' ) ) : null;

        if ( isset( $items->date ) ) {
            $date = explode("~", $items->date);
            $start_date = $date[0];
            $end_date = $date[1];
            $query->whereBetween('created_at', [$start_date.' 00:00:00', $end_date.' 23:59:59']);
        }

        return $query;
    }

    public function dataTableOptions(): Collection
    {
        $expect = ['page', 'groupBy', 'groupDesc', 'mustSort', 'multiSort'];

        return collect( json_decode( request()->input( 'options' ), true ) )->except( $expect );
    }

    public function delete($id)
    {
        $rank = Rank::findOrFail($id);

        $rank->delete();

        return $this->response(200, [
            'record' => $rank,
        ]);
    }
}
