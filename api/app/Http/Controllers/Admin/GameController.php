<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Game;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use App\Http\Controllers\Admin\Traits\Searchable;
use App\Http\Resources\GameCollection;

class GameController extends Controller
{
    use Searchable;
    /**
     * Display a listing of the Customer.
     *
     * @return Response
     */
    public function index()
    {
        $options = $this->dataTableOptions();

        $query = $this->prepareSearchQuery(Game::query()->with(['customer', 'level']), $options);

        $records = $query->paginate($options->get('itemsPerPage'));

        return $this->response( 200, [
            'records' => new GameCollection( $records ),
        ] );
    }

    /**
     * Prepare search query builder instance
     *
     * @param Collection $options
     * @return Builder
     */
    public function prepareSearchQuery(Builder $query, Collection $options): Builder
    {
        $query = $this->searchQuery($query);
        $options = $this->dataTableOptions();

        $orderBy = collect($options->get('sortBy'))->combine($options->get('sortDesc'));
        $orderBy->map(function ($isDesc, $key) use ($query) {
            $key = preg_match('/\./', $key) ? sprintf('%s_id', explode('.', $key)[0]) : $key;
            $query->orderBy($key, $isDesc ? 'desc' : 'asc');
        });

        return $query;
    }

    public function searchQuery(Builder $query)
    {
        $items = !empty(request()->input('search')) ? json_decode(request()->input('search')) : null;

        if (isset($items->level_id)) {
            $query->whereHas('level', function (Builder $query) use ($items) {
                $query->where('id', $items->level_id);
            });
        }

        if (isset($items->customer_name)) {
            $query->whereHas('customer', function (Builder $query) use ($items) {
                $query->where('name', 'LIKE', "%{$items->customer_name}%");
            });
        }


        return $query;
    }

    public function dataTableOptions(): Collection
    {
        $expect = ['page', 'groupBy', 'groupDesc', 'mustSort', 'multiSort'];

        return collect(json_decode(request()->input('options'), true))->except($expect);
    }
}
