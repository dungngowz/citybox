<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdminResource;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->guard = 'admin';

        $this->middleware( 'auth:' . $this->guard, ['except' => ['login']] );
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $validator = Validator::make( request()->all(), [
            'email'    => 'required|email|max:255',
            'password' => [
                'required',
                'string',
                'min: 8',
                'max: 20',
            ],
        ] );
        if ( $validator->fails() ) {
            return $this->response( 422, [], '', $validator->errors() );
        }

        $credentials = request( ['email', 'password'] );

        if ( !$token = auth( $this->guard )->attempt( $credentials ) ) {
            return $this->response( 401, [], __( 'text.account_login_invalid' ) );
        }

        $user = auth( $this->guard )->user();

        return $this->response( 200, [
            'token'      => $token,
            'token_type' => 'bearer',
            'user'       => new AdminResource( $user ),
        ] );
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth( $this->guard )->user();

        return $this->response( 200, [
            'user' => new AdminResource( $user ),
        ] );
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json( ['message' => 'Successfully logged out.'] );
    }
}
