<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the Customer.
     *
     * @return Response
     */
    public function index()
    {
        $records = Setting::whereNull('deleted_at')->firstOrFail();

        if (!$records) {
            return $this->response(204, [], __('text.this_resource_is_not_available'));
        }

        return $this->response(200, [
            'records' => $records,
        ]);
    }

    public function update(Request $request, $type)
    {
        $setting = Setting::whereNull('deleted_at')->firstOrFail();

        if (!$setting) {
            return $this->response(204, [], __('text.this_resource_is_not_available'));
        }

        $misson = $request->input('misson');

        $setting->$type = $misson;
        $setting->save();

        return $this->response(200, [
            'records' => $setting,

        ], __('text.this_resource_is_updated_success'));
    }
}
