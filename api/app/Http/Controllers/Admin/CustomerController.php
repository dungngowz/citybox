<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CsvExport;
use App\Http\Controllers\Admin\Traits\Searchable;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerCollection;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{
    use Searchable;
    /**
     * Display a listing of the Customer.
     *
     * @return Response
     */
    public function index()
    {
        $options = $this->dataTableOptions();

        $query = $this->prepareSearchQuery( Customer::query(), $options );

        $records = $query->orderBy('id', 'desc')->paginate( $options->get( 'itemsPerPage' ) );

        return $this->response( 200, [
            'records' => new CustomerCollection( $records ),
        ] );
    }

    /**
     * CSV download
     *
     * @param  Customer   $customer
     * @return Response
     */
    public function csv()
    {
        $options = $this->dataTableOptions();
        $query   = $this->prepareSearchQuery( Customer::query(), $options );

        return Excel::download( new CsvExport( $query, 'exports.customers' ), '_.csv' );
    }

    /**
     * Prepare search query builder instance
     *
     * @param  Collection $options
     * @return Builder
     */
    public function prepareSearchQuery(
        Builder $query,
        Collection $options
    ): Builder {
        $query   = $this->searchQuery( $query );
        $options = $this->dataTableOptions();

        $orderBy = collect( $options->get( 'sortBy' ) )->combine( $options->get( 'sortDesc' ) );
        $orderBy->map( function (
            $isDesc,
            $key
        ) use ( $query ) {
            $key = preg_match( '/\./', $key ) ? sprintf( '%s_id', explode( '.', $key )[0] ) : $key;
            $query->orderBy( $key, $isDesc ? 'desc' : 'asc' );
        } );

        return $query;
    }

    public function searchQuery( Builder $query )
    {
        $items = !empty( request()->input( 'search' ) ) ? json_decode( request()->input( 'search' ) ) : null;

        if ( isset( $items->name ) ) {
            $query->where( 'name', 'LIKE', "%{$items->name}%" );
        }

        if ( isset( $items->email ) ) {
            $query->where( 'email', 'LIKE', "%{$items->email}%" );
        }

        if ( isset( $items->phone ) ) {
            $query->where( 'phone', 'LIKE', "%{$items->phone}%" );
        }

        return $query;
    }

    public function dataTableOptions(): Collection
    {
        $expect = ['page', 'groupBy', 'groupDesc', 'mustSort', 'multiSort'];

        return collect( json_decode( request()->input( 'options' ), true ) )->except( $expect );
    }

    public function delete($id)
    {
        $customer = Customer::findOrFail($id);

        $customer->delete();

        return $this->response(200, [
            'record' => $customer,
        ]);
    }
}
