<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SolutionCollection;
use App\Models\Solution;
use Illuminate\Http\Request;
class SolutionController extends Controller
{
    /**
     * Display a listing of the Customer.
     *
     * @return Response
     */
    public function index()
    {
        $records = Solution::query();

        $options = $this->dataTableOptions();

        if (!$records) {
            return $this->response(204, [], __('text.this_resource_is_not_available'));
        }

        $records = $records->paginate($options->get('itemsPerPage'));

        return $this->response( 200, [
            'records' => new SolutionCollection ($records)
        ] );

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $solution = Solution::findOrFail($id);

        $solution->update($data);

        return $this->response( 200, [
            'records' => $solution
        ] );
    }

    public function dataTableOptions()
    {
        $expect = ['page', 'groupBy', 'groupDesc', 'mustSort', 'multiSort'];

        return collect(json_decode(request()->input('options'), true))->except($expect);
    }
}
