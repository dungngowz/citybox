<?php

namespace App\Http\Controllers\Admin\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

trait Searchable
{
    /**
     * Query builder methods allowed by search term
     *
     * @var array
     */
    protected static $allow_methods = [
        'where',
        'whereIn',
        'whereNotIn',
        'whereBetween',
        'whereNotBetween',
        'whereNull',
        'whereNotNull',
        'whereDate',
        'whereMonth',
        'whereDay',
        'whereYear',
        'whereTime',
        'whereColumn',
    ];

    /**
     * Add search conditions to the query builder
     *
     * @param Builder $query
     * @return Builder
     */
    protected static function addSearchConditions(Builder $query): Builder
    {
        $search = collect(request()->input('search') ?? []);
        $search->each(function ($term) use ($query) {
            $term = collect(json_decode($term, true));
            if (count($term->get('columns')) === 1) {
                self::buildCondition($query, $term);
            } else {
                $query->where(function ($query) use ($term) {
                    self::buildCondition($query, $term);
                });
            }
        });

        return $query;
    }

    /**
     * Building search condition from search term
     *
     * @param Builder $query
     * @param Collection $term
     * @return void
     */
    private static function buildCondition(Builder $query, Collection $term): void
    {
        $method = $term->get('method');
        $columns = $term->get('columns');
        $value = $term->get('value');
        $operator = $term->get('operator');

        if (!in_array($method, self::$allow_methods)) {
            return;
        }
        foreach ($columns as $key => $column) {
            $column = preg_replace('/(_gte?|_lte?)/', '', $column);
            if ($key >= 1) {
                $method = preg_replace('/where/', 'orWhere', $method);
            }

            if (preg_match('/\./', $column)) {
                [$relation, $column] = explode('.', $column);
                $query->whereHas($relation, function ($query) use ($method, $column, $value, $operator) {
                    self::addMethod($query, $method, $column, $operator, $value);
                });
            } else {
                self::addMethod($query, $method, $column, $operator, $value);
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $query
     * @param [type] $method
     * @param [type] $column
     * @param [type] $operator
     * @param [type] $value
     * @return void
     */
    private static function addMethod($query, $method, $column, $operator, $value)
    {
        if ($operator) {
            $query->$method($column, $operator, $operator === 'LIKE' ? sprintf('%%%s%%', $value) : $value);
        } else {
            $query->$method($column, $value);
        }
    }
}
