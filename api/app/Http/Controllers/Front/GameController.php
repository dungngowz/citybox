<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Rank;
use Illuminate\Http\Request;

class GameController extends Controller
{
    protected $currentUser;

    public function __construct()
    {
        $this->guard       = 'front';
        $this->currentUser = auth()->guard($this->guard)->user();
    }

    /**
     * Api: {{DOMAIN}}/front/get-current-level
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllGame()
    {
        $game = NULL;
        if ($this->currentUser) {
            $game = Game::where('customer_id', $this->currentUser->id)
                ->orderBy('level_id', 'desc')
                ->first();
        }

        return $this->response(200, [
            'record' => $game,
        ]);
    }

    public function getGame($level_id)
    {
        $game = NULL;
        if ($this->currentUser) {
            $game = Game::where('customer_id', $this->currentUser->id)
                ->where('level_id', $level_id)
                ->orderBy('level_id', 'desc')
                ->first();
        }

        return $this->response(200, [
            'record' => $game,
        ]);
    }

    public function updateGame(Request $request)
    {
        $data = $request->all();

        $removeItem = $data['removeItem'] ?? 'false';

        $game = Game::where('customer_id', $this->currentUser->id)
            ->orderBy('level_id', 'desc')
            ->first();

        $checkDone = false;

        if ($game->level_id == 4 && $game->is_done == 1) {
            $checkDone = true;

            $game->update([
                'coins_after_play' => 0
            ]);
        }

        if ($removeItem == 'false' && !$checkDone) {
            if (isset($data['is_play_again'])) {
                $data['coins_after_play'] = $game->coins_berore_play;
            }

            $is_cheat = false;
            if(isset($data['level_id']))
            {
                switch ($data['level_id']) {
                    case 1:
                        if ($data['times'] < 6) {
                            $is_cheat = true;
                        }
                        break;
                    case 2:
                        if ($data['times'] < 32) {
                            $is_cheat = true;
                        }
                        break;
                    case 3:
                        if ($data['times'] < 38) {
                            $is_cheat = true;
                        }
                        break;
                    case 4:
                        if ($data['times'] < 54) {
                            $is_cheat = true;
                        }
                        break;
                }
            }

            if ($is_cheat === true) {
                $this->currentUser->update([
                    'is_cheat' => 1
                ]);
            }


            $game->update($data);

            if ($data['is_done'] == 1) {
                $currentTime = $this->currentUser->times ?? 0;
                $times = $game->times;

                $this->currentUser->update([
                    'times' => $times
                ]);

                if ($game->level_id == 4) {
                    $times1 = Game::where('customer_id', $this->currentUser->id)->where('level_id', 1)->first()->times;
                    $times2 = Game::where('customer_id', $this->currentUser->id)->where('level_id', 2)->first()->times;
                    $times3 = Game::where('customer_id', $this->currentUser->id)->where('level_id', 3)->first()->times;
                    $times4 = $game->times;

                    if ($times4 < $times3 || $times3 < $times2 || $times2 < $times1 || $times4 < 54) {
                        $this->currentUser->update([
                            'is_cheat' => 1
                        ]);
                    } else {
                        $this->currentUser->update([
                            'is_cheat' => 0
                        ]);
                    }

                    $rank_times = Rank::where('customer_id', $this->currentUser->id)->orderBy('id', 'desc')->first();
                    $rank_times->update([
                        'times' => $this->currentUser->times
                    ]);
                }
            }

            if ($data['is_done'] == 1 && $game->level_id < 4) {
                $data['level_id'] = $game->level_id + 1;
                $data['coins_berore_play'] = 100;
                $data['coins_after_play'] = 100;
                $data['meta_data'] = NULL;
                $data['is_done'] = 0;
                $data['ip'] = $request->ip();

                $this->currentUser->games()->create($data);
            }
        } else if ($removeItem == 'true') {
            $game->update($data);
        }

        return $this->response(200, [
            'record' => $game,
        ]);
    }

    public function updateSolution(Request $request)
    {
        $solution = $request->input('solution');

        $game = Game::where('customer_id', $this->currentUser->id)
            ->orderBy('level_id', 'desc')
            ->first();

        $game->update([
            'solution' => $solution,
        ]);

        return $this->response(200, [
            'record' => $game,
        ]);
    }

    public function addCoins()
    {
        $game = Game::where('customer_id', $this->currentUser->id)
            ->orderBy('level_id', 'desc')
            ->first();

        $coins = (int)$game->coins_after_play + 300;

        $game->update([
            'coins_after_play' => $coins
        ]);


        return $this->response(200, [
            'record' => $game,
        ]);
    }
}
