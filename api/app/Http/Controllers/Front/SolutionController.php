<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Solution;
use Illuminate\Http\Request;

class SolutionController extends Controller
{
    public function getSolution($id)
    {
        $solutions = Solution::where('level_id', $id)->get();

        return $this->response(200, [
            'record' => $solutions,
        ]);
    }

    public function checkSolution(Request $request)
    {
        $solution = Solution::findOrFail($request->input('answer_id'));

        $is_correct = $solution->is_correct;

        return $is_correct;
    }
}
