<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->guard = 'front';

        $this->middleware( 'auth:' . $this->guard, ['except' => ['loginSocial']] );
    }

    /**
     * Api: {{DOMAIN}}/front/auth/login-social
     *
     * @return \Illuminate\Http\Response
     */
    public function loginSocial( Request $request )
    {
        \Log::debug(json_encode($request->all()));
        $paramsRequest = $request->all();

        //Validate data
        $validator = Validator::make( $paramsRequest, [
            'access_token' => 'required',
            'type_social'  => 'required|in:facebook,kakao,naver,apple',
        ] );
        if ( $validator->fails() ) {
            return $this->response( 422, [], __( 'text.invalid_data' ), $validator->errors() );
        }

        $accessToken = $request->access_token;

        //Get info by access token
        if ( $request->type_social == 'apple' ) {
            $token        = explode( '.', $accessToken );
            $token        = $token[1];
            $idToken      = base64_decode( $token );
            $tokenData    = json_decode( $idToken );
            $responseData = (array) $tokenData;

            $responseData['token'] = $accessToken;
            $responseData['id']    = $responseData['sub'];
        } else {
            if ( $request->type_social == 'facebook' ) {
                $urlRequest = "https://graph.facebook.com/v3.1/me?fields=id,last_name,first_name,email,birthday&access_token=$accessToken";
                $response   = Http::get( $urlRequest );
            } else if ( $request->type_social == 'kakao' ) {
                $urlRequest = 'https://kapi.kakao.com/v2/user/me';
                $response   = Http::withHeaders( [
                    'Authorization' => 'Bearer ' . $accessToken,
                    'Accept'        => 'application/json',
                ] )->post( $urlRequest );
            } else if ( $request->type_social == 'naver' ) {
                $urlRequest = 'https://openapi.naver.com/v1/nid/me';
                $response   = Http::withHeaders( [
                    'Authorization' => 'Bearer ' . $accessToken,
                    'Accept'        => 'application/json',
                ] )->post( $urlRequest );
            }

            //Validate response
            if ( $response->failed() ) {
                return $this->response( 422, [], __( 'text.the_access_token_is_invalid' ) );
            }

            $responseData = json_decode( $response->getBody()->getContents(), true );

            //Get email if exists
            if ( $request->type_social == 'kakao' ) {
                if ( !empty( $responseData['kakao_account'] ) ) {
                    if ( !empty( $responseData['kakao_account']['email'] ) ) {
                        $responseData['email'] = $responseData['kakao_account']['email'];
                    }

                    if ( !empty( $responseData['kakao_account']['profile'] ) ) {
                        $responseData['first_name'] = $responseData['kakao_account']['profile']['nickname'];
                    }
                }
            } else if ( $request->type_social == 'naver' ) {
                $responseData = $responseData['response'];
            }
        }

        $responseData['token']       = $accessToken;
        $responseData['id_social']   = $responseData['id'];
        $responseData['type_social'] = $request->type_social;

        $passDefault              = env( 'PASSWORD_DEFAULT', 's5d123ds14' );
        $responseData['password'] = Hash::make( $passDefault );

        if ( !isset( $responseData['email'] ) ) {
            $responseData['email'] = $responseData['id'] . '@' . trim( $request->type_social ) . '.com';
        }

        $responseData['username'] = explode( '@', $responseData['email'] )[0];

        if ( empty( $responseData['first_name'] ) ) {
            $responseData['name'] = $responseData['username'];
        } else {
            $responseData['name'] = $responseData['first_name'] . ' ' . $responseData['last_name'];
        }

        //Check customer is exists
        $user = Customer::where( 'id_social', $responseData['id_social'] )->first();
        if ( !$user ) {
            $game = [];
            if(isset($request['customer_tmp'])) {
                $data = json_decode($request['customer_tmp'], true);
                $coins = (int)$data['coins'];
                $game = [
                    'level_id' => $data['level'],
                    'coins_berore_play' => $coins,
                    'coins_after_play' => $coins,
                    'times' => $data['times'],
                    'meta_data' => $data['meta_data'],
                    'is_done' => 0,
                    'ip' => $request->ip()
                ];
                $responseData['coins'] = $coins;
                $responseData['actor_name'] = $data['name'];
                $responseData['gender'] = $data['gender'];
                $responseData['ip'] = $request->ip();

            } else {
                $game = [
                    'level_id' => 1,
                    'coins_berore_play' => 100,
                    'coins_after_play' => 100,
                    'is_done' => 0,
                    'ip' => $request->ip()
                ];

                $responseData['coins'] = 100;
            }
            $user = Customer::create( $responseData );

            $user->games()->create($game);
            $user->ranks()->create([
                'times' => NULL
            ]);
        }else if($user && isset($request['customer_tmp'])) {
            $data = json_decode($request['customer_tmp'], true);
            $coins = (int)$data['coins'];

            $user->coins = $coins;
            $user->actor_name = $data['name'];
            $user->gender = $data['gender'];
            $user->ip = $request->ip();
            $user->save();

            $game = [
                'level_id' => $data['level'],
                'coins_berore_play' => $coins,
                'coins_after_play' => $coins,
                'times' => $data['times'],
                'meta_data' => $data['meta_data'],
                'is_done' => 0,
                'ip' => $request->ip()
            ];

            $user->games()->create($game);
            $user->ranks()->create([
                'times' => NULL
            ]);
        }

        //Render jwtToken
        $credentials = ['email' => $user->email, 'id_social' => $responseData['id_social'], 'password' => $passDefault];
        $jwtToken    = auth( $this->guard )->attempt( $credentials );

        if ( !$jwtToken ) {
            return $this->response( 401, [], __( 'text.this_account_is_fake' ) ); //This account is fake
        }

        return $this->response( 200, [
            'token'      => $jwtToken,
            'token_type' => 'bearer',
            'user'       => new CustomerResource( $user ),
        ] );
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth( $this->guard )->user();

        return $this->response( 200, [
            'user' => new CustomerResource( $user ),
        ] );
    }
}
