<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
        /**
     * Display a listing of the Customer.
     *
     * @return Response
     */
    public function index()
    {
        $records = Setting::whereNull('deleted_at')->firstOrFail();

        if (!$records) {
            return $this->response(204, [], __('text.this_resource_is_not_available'));
        }

        return $this->response(200, [
            'records' => $records,
        ]);
    }
}
