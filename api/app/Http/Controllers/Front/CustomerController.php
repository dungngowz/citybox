<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use App\Models\CustomerTmp;
use App\Models\Game;
use App\Models\Rank;
use App\Services\SmsService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;

class CustomerController extends Controller
{

    protected $currentUser;

    public function __construct()
    {
        $this->guard       = 'front';
        $this->currentUser = auth()->guard($this->guard)->user();
    }

    public function getRank()
    {
        $ranks = Rank::orderByDesc('times')
        ->where([
            ['created_at', '>=', '2021-11-08 19:30:00'],
            ['created_at', '<=', '2021-12-05 23:59:59'],
        ])
        ->whereNotNull('times')
        ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
        ->whereHas('customer', function ($q) {
            $q->where('is_cheat', 0);
        })
        ->with('customer')
        ->get();

        return $this->response(200, [
            'ranks' => $ranks,
        ]);
    }

    public function getCurrentCustomer()
    {
        $customer = Customer::where('id', auth()->guard($this->guard)->user()->id)->with('ranks')->first();

        return $this->response(200, [
            'customer' => $customer
        ]);
    }

    public function updateInfo(Request $request)
    {
        $data = $request->all();

        $type = $data['type'] ?? 1;

        $data_validate = $type == 1 ? [
            'actor_name'    => 'required|max:255',
            'gender' => 'required'
        ] : [
            'email'    => 'required|email|max:255',
            'phone' => 'required|numeric|unique:customers|regex:/^[0-9]{10,11}$/i',
            'name' => 'required'
        ];

        $validator = Validator::make($data, $data_validate);

        if ($validator->fails()) {
            return $this->response(422, [], '', $validator->errors());
        }

        $smsStatus = false;

        try {
            // sms
            $countSent = Customer::where('sms_sent', 1)->count();

            if ($countSent < 2000 && empty($this->currentUser->sms_sent) && $type == '2') {
                $sms = new SmsService();
                $smsStatus = $sms->topUp($data['phone']);

                if ($smsStatus) {
                    $this->currentUser->sms_sent = 1;
                }
            }
        } catch (Exception $ex) {
            //
        }

        $user = $this->currentUser->update($data);

        $resp = [
            'record' => $user,
        ];

        if ($smsStatus == false) {
            $resp['out_off_money'] = true;
        }

        return $this->response(200, $resp);
    }

    public function checkName(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:customers_tmp|max:255'
        ]);

        if ($validator->fails()) {
            return $this->response(422, [], '', $validator->errors());
        }

        $customer_tmp = CustomerTmp::create([
            'name' => $request->input('name')
        ]);



        return $this->response(200, [
            'record' => $customer_tmp,
        ]);
    }

    public function playAgain()
    {
        $user = auth()->guard($this->guard)->user();

        Game::where('customer_id', $user->id)->each(function ($customer, $key) {
            $customer->delete();
        });

        // $newUser = $user->replicate();

        // $newUser->times = NULL;
        // $newUser->save();

        // $user->delete();

        $game = [
            'level_id' => 1,
            'coins_berore_play' => 100,
            'coins_after_play' => 100,
            'times' => NULL,
            'meta_data' => NULL,
            'is_done' => 0,
        ];

        $user->times = NULL;
        $user->save();

        $user->games()->create($game);
        $user->ranks()->create([
            'times' => NULL
        ]);

        return $this->response(200, [
            'record' => $user,
        ]);
    }
}
