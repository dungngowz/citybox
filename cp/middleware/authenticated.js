import { defineNuxtMiddleware } from '@nuxtjs/composition-api'

export default defineNuxtMiddleware(({ $auth, store }) => {
  const user = $auth.user
  store.commit('authentication/updateUser', user)
})
