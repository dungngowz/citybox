import {
  reactive,
  watch,
  useContext,
  useAsync,
  inject,
} from '@nuxtjs/composition-api'

export default function useDataTable(resource, autoFetch = true) {
  const { $axios, $moment } = useContext()

  const localState = process.client
    ? JSON.parse(localStorage.getItem(resource) || '{}')
    : ''

  const state = reactive({
    items: [],
    total: 0,
    options: {
      page: 1,
      itemsPerPage: 10,
      sortBy: ['id'],
      sortDesc: [false],
      groupBy: [],
      groupDesc: [],
      multiSort: false,
      mustSort: false,
    },
    footerProps: {
      showCurrentPage: true,
      showFirstLastPage: true,
      itemsPerPageOptions: [20, 50, 100],
    },
    search: {},
    searchTerms: {},
    ...localState,
  })

  const convertNameItem = inject('convertNameItem')

  const paginationHandler = async () => {
    const { data: pager } = await $axios.get(resource, {
      params: {
        page: state.options.page,
        options: state.options,
        search: state.search,
      },
    })
    state.total = pager.data?.records.meta.total
    state.items = reactive(pager.data?.records.data)
    if (resource === '/admin/games') {
      state.items.forEach((item) => {
        item.meta_data = convertNameItem(
          item.levels?.id ?? 1,
          JSON.parse(item.meta_data)
        )
      })
    }
    if (resource === '/admin/customers') {
      state.items.forEach((item) => {
        item.created_at = item.created_at_display
      })
    }
    if (process.client) {
      localStorage.setItem(resource, JSON.stringify({ ...state, items: [] }))
    }
  }

  const dataOptionsHandler = async () => await paginationHandler()

  const search = async () => {
    state.options.page = 1

    await dataOptionsHandler()
  }

  const csvDownload = async (name) => {
    state.csvDownloading = true
    const { data } = await $axios.get(`${resource}/csv`, {
      params: {
        page: state.options.page,
        options: state.options,
        search: state.search,
      },
      responseType: 'blob',
    })
    const bom = new Uint8Array([0xef, 0xbb, 0xbf])
    const blob = new Blob([bom, data], { type: 'text/csv' })
    const link = document.createElement('a')
    link.href = window.URL.createObjectURL(blob)
    link.setAttribute(
      'download',
      `${name}_${$moment().format('YYYYMMDD-HHmm')}.csv`
    )
    link.click()
    state.csvDownloading = false
  }

  watch(() => state.options, dataOptionsHandler)

  if (autoFetch) {
    useAsync(async () => await paginationHandler())
  }

  return {
    state,
    dataOptionsHandler,
    search,
    csvDownload,
  }
}
