import colors from 'vuetify/es5/util/colors'
require('dotenv').config()

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: true,
  server: {
    port: 3001 // default: 3000
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s',
    title: 'CityBox',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/vue-fullscreen.js',
    '~/plugins/mixins/validation',
    '~/plugins/mixins/user',
    '~/plugins/axios',
    '~/plugins/mixins/index.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    //https://composition-api.nuxtjs.org/
    '@nuxtjs/composition-api/module',
    '@nuxtjs/moment',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    [
      "vue-toastification/nuxt",
      {
        timeout: 5000,
        draggable: true,
        closeOnClick: false
      }
    ]
  ],

  axios: {
    // proxy: true,
    retry: true,
    baseURL: process.env.SERVER_URL + '/api'
  },

  //Auth configuration
  auth: {
    strategies: {
      local: {
        token: {
          property: 'data.token',
          global: true,
        },
        user: {
          property: 'data.user',
          // autoFetch: true
        },
        endpoints: {
          login: {
            url: 'admin/auth/login', method: 'post'
          },
          user: {
            url: 'admin/auth/user', method: 'get'
          },
          logout: {
            url: 'admin/auth/logout', method: 'get'
          }
        },
      }
    },
    redirect: {
      login: '/authentication/login'
    }
  },

  router: {
    middleware: ['auth']
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#1C3FAC',
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.red.accent4,
          success: colors.green.accent3
        },
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
