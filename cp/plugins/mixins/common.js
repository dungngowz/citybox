export const commonMethod = {
    methods: {
        addOptionAllToSelect(items) {
            let itemNull = [{id: null, name: '---'}];
            return itemNull.concat(items)
        },
        linkToRoute(link, hash = ""){
            this.$router.push({path: link, hash: hash});
        }
    }
}