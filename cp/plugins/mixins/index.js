import { onGlobalSetup, provide } from '@nuxtjs/composition-api'
import Toastify from "toastify-js"

export default () => {
  onGlobalSetup(() => {
    const convertNameItem = (levelId, metaData) => {
      let items = []

      if (levelId === 1) {
        items = [
          'Bồn tắm',
          'Bồn vệ sinh',
          'Bồn rữa mặt',
          'Tủ đồ',
          'Gương',
          'Vòi sen',
        ]
      } else if (levelId === 2) {
        items = ['Tủ bếp', 'Bàn ăn', 'Thảm sàn', 'Kệ bếp', 'Kệ chén', 'Tủ lạnh']
      } else if (levelId === 3) {
        items = [
          'Giường ngủ',
          'Chăn gối',
          'Nệm',
          'Tủ đồ',
          'Đèn ngủ',
          'Bàn trang điểm',
        ]
      } else if (levelId === 4) {
        items = ['Sofa', 'Bàn sofa', 'Kệ sách', 'Tivi', 'Kệ tivi', 'Chậu cây']
      }

      let startPoint = 5
      if (levelId === 2) {
        startPoint = 11
      } else if (levelId === 3) {
        startPoint = 17
      } else if (levelId === 4) {
        startPoint = 23
      }

      const data = []
      metaData.forEach((item) => {
        switch (item) {
          case startPoint:
            data.push(items[0])
            break
          case startPoint + 1:
            data.push(items[1])
            break
          case startPoint + 2:
            data.push(items[2])
            break
          case startPoint + 3:
            data.push(items[3])
            break
          case startPoint + 4:
            data.push(items[4])
            break
          case startPoint + 5:
            data.push(items[5])
            break
          default:
          // code block
        }
      })
      return data
    }

    const notify = (message, flag) => {
      Toastify({ text: message || '', backgroundColor: flag ? "#91C714" : "#D32929", style: { borderRadius: '0.75rem' } }).showToast()
    }

    provide('toastify', notify)
    provide('convertNameItem', convertNameItem)
  })
}
