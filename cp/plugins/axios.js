export default function ({ $axios, store, $toast }) {
  $axios.onError(error => {
    if (error.response.status !== 200) {
      if (Object.keys(error.response.data.errors).length > 0) {
        store.dispatch('validation/setErrors', error.response.data.errors)
      } else {
        $toast.error(error.response.data.message)
      }
    }

    return Promise.reject(error);
  })

  $axios.onRequest(() => {
    store.dispatch('validation/clearErrors')
  })
}