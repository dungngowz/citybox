export const state = () => ({
  isFullscreen: false,
  isExpandDrawer: false
})

export const mutations = {
  toggleFullscreen(state) {
    state.isFullscreen = !state.isFullscreen;
  },
  toggleDrawer(state) {
    state.isExpandDrawer = state.isExpandDrawer === true ? 1 : true;
  }
}