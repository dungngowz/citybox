import Toastify from "toastify-js"
import "toastify-js/src/toastify.css"

import { onGlobalSetup, provide } from '@nuxtjs/composition-api'

export default () => {
    onGlobalSetup(() => {
        const notify = (message, flag) => {
          Toastify({ text: message || '', backgroundColor: flag ? "#91C714" : "#D32929" }).showToast()
        }
        provide('toastify', notify)
    })
}