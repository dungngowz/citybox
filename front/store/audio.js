export const state = () => ({
  isMuted: process.client ? localStorage.getItem('isMuted') : false || false,
})

export const getters = {
}

export const mutations = {
  setMuted(state, payload) {
    state.isMuted = !state.isMuted
    if(process.client) {
      localStorage.setItem('isMuted', state.isMuted)
    }
  },
}

export const actions = {
}
