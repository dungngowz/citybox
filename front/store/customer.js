export const state = () => ({
    level: 1,
    coins: 0
})

export const getters = {
    getLevel: (state) => `0${state.level}`,
    getCoins: (state) => state.coins
}

export const mutations = {
    SET_LEVEL(state, payload ) {
        state.level = payload
    },

    SET_COINS(state, payload ) {
        state.coins = payload
    }
}

export const actions = {
}
