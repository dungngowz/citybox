import { defineNuxtMiddleware } from '@nuxtjs/composition-api'

export default defineNuxtMiddleware(({ $storage, $auth, route, redirect }) => {

  if (!['bang-xep-hang'].includes(route.name)) {
    if (!$storage.getUniversal('customer_tmp') && !$auth.user) {
      if (!['chon-nhan-vat', 'index'].includes(route.name)) {
        redirect('/')
      }
    } else if (['chon-nhan-vat', 'index'].includes(route.name) && $storage.getUniversal('is_view_character')) {
      redirect('/chon-phong')
    }else if(route.name == 'index' && $auth.user){
      redirect('/chon-phong')
    }
  }
})
