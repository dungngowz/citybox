import colors from 'vuetify/es5/util/colors'

export default {
  server: {port: 3003},
  pageTransition: 'page',
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s',
    title: 'Game City Box mua sắm trang trí nhà cửa - Tổng giá trị giải thưởng lên đến 95,000,000đ. Tham gia ngay! ',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { hid: 'og:image', property: 'og:image', content: 'https://thaidockhongkhi.vn/meta-img.png' },
      { hid: 'og:image:secure_url', property: 'og:image:secure_url', content: 'https://thaidockhongkhi.vn/meta-img.png' },
      { hid: 'og:image:secure ', property: 'og:image:secure', content: 'https://thaidockhongkhi.vn/meta-img.png' },
      { hid: 'og:title', property: 'og:title', content: 'Game City Box mua sắm trang trí nhà cửa - Tổng giá trị giải thưởng lên đến 95,000,000đ. Tham gia ngay!' },
    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~plugins/vue-kinesis', ssr: false },
    { src: '~plugins/vue-social-sharing', ssr: false },
    '~/plugins/utils/index.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    { path: '~/components/common/' },
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    //'@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    //'@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/composition-api
    '@nuxtjs/composition-api/module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/universal-storage',
    '@nuxtjs/auth-next',
    '@nuxtjs/gtm',
  ],

  gtm: {
    id: 'GTM-WHZMBJ3'
  },


  auth: {
    strategies: {
      facebook: {
        endpoints: {
          userInfo: 'https://graph.facebook.com/v6.0/me?fields=id,name'
        },
        clientId: '3174126086149460',
        scope: ['public_profile', 'email']
      },
      local: {
        token: {
          property: 'data.token',
          global: true,
        },
        user: {
          property: 'data.user',
          // autoFetch: true
        },
        endpoints: {
          login: {
            url: 'front/auth/login-social', method: 'post'
          },
          user: {
            url: 'front/auth/user', method: 'get'
          },
          logout: {
            url: 'front/auth/logout', method: 'get'
          }
        },
      }
    }
  },

  axios: {
    // proxy: true,
    retry: true,
    baseURL: process.env.SERVER_URL + '/api'
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/css/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}

